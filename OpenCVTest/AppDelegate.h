//
//  AppDelegate.h
//  OpenCVTest
//
//  Created by Konstantin on 14.04.16.
//  Copyright © 2016 Konstantin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


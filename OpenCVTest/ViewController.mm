//
//  ViewController.m
//  OpenCVTest
//
//  Created by Konstantin on 14.04.16.
//  Copyright © 2016 Konstantin. All rights reserved.
//

#import "ViewController.h"
#import <opencv2/opencv.hpp>
#import <opencv2/videoio/cap_ios.h>

@interface ViewController () <CvVideoCameraDelegate> {
    IBOutlet UIImageView *imageView;
}

@property (nonatomic, strong) CvVideoCamera *videoCamera;

@end


@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCamera];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)actionStart:(id)sender {
    [self.videoCamera start];
}

- (IBAction)turn:(id)sender {
    [self.videoCamera switchCameras];
}

- (void)setupCamera {
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = self;
}



#pragma mark - Protocol CvVideoCameraDelegate

#ifdef __cplusplus
- (void)processImage:(cv::Mat &)image {
//    [self makeGray:image];
    [self makeCanny:image];
}

- (void)makeGray:(cv::Mat &)image {
    
    cv::Mat image_copy;
    cvtColor(image, image_copy, CV_BGRA2BGR);
    
    bitwise_not(image_copy, image_copy);
    cvtColor(image_copy, image, CV_BGR2BGRA);
}

- (void)makeCanny:(cv::Mat &)image {

    cv::Mat changedImage;
    
    Canny(image, changedImage, 50, 200, 3);
    
    image = changedImage;
}

#endif

@end

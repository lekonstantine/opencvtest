//
//  main.m
//  OpenCVTest
//
//  Created by Konstantin on 14.04.16.
//  Copyright © 2016 Konstantin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
